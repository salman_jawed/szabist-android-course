package szabist.com.persistence;

/**
 * Created by salmanjawed on 05/11/2017.
 */

public class AppConstants {

    public static final String MY_APP_PREF = "com.szabist.persistence";
    public static final String USER_MODEL_CACHE_KEY = "usermodel";
}
