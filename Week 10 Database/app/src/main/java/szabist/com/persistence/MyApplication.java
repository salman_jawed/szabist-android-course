package szabist.com.persistence;

import android.app.Application;
import android.content.Context;

/**
 * Created by salmanjawed on 16/10/2015.
 */
public class MyApplication extends Application {

    public static Context appContext;

    private static MyApplication instance;

    public static MyApplication getInstance() {
        if (instance == null) {
            instance = new MyApplication();
        }
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = getApplicationContext();
        instance = this;
    }

    public static Context getAppContext() {
        return appContext;
    }
}
