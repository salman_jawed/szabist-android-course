package szabist.com.persistence.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import szabist.com.persistence.R;
import szabist.com.persistence.usermanager.User;
import szabist.com.persistence.usermanager.UserManager;

/**
 * Created by salmanjawed on 05/11/2017.
 */

public class HomeActivity extends AppCompatActivity {

    TextView tvName, tvEmail;
    Button btnLogout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        tvName = findViewById(R.id.tv_name);
        tvEmail = findViewById(R.id.tv_email);


        User mUser = UserManager.getInstance(this).getUserProfile();


        tvName.setText(mUser.getUserName());
        tvEmail.setText(mUser.getUserEmail());

        btnLogout = findViewById(R.id.btn_logout);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserManager.getInstance(HomeActivity.this).removeUserProfileJSON();
                Intent i = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });


    }
}
