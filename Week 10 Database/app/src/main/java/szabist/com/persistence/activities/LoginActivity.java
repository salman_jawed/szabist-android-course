package szabist.com.persistence.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import szabist.com.persistence.R;
import szabist.com.persistence.usermanager.User;
import szabist.com.persistence.usermanager.UserManager;
import szabist.com.persistence.utils.ValidationUtils;

public class LoginActivity extends AppCompatActivity {

    EditText etUsername;
    EditText etPassword;

    Button btnLogin, btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUsername = findViewById(R.id.et_username);
        etPassword = findViewById(R.id.et_password);

        btnLogin = findViewById(R.id.btn_login);
        btnRegister = findViewById(R.id.btn_register);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkValidation()) {
                    List<User> users = User.listAll(User.class);
                    if (users.size() > 0) {
                        User mUser = checkUser(users);
                        if (mUser != null) {
                            UserManager.getInstance(LoginActivity.this).cacheUserProfileJSON(mUser);
                            Intent i = new Intent(LoginActivity.this, HomeActivity.class);
                            startActivity(i);
                            finish();
                        } else {
                            Toast.makeText(LoginActivity.this, "Invalid credentails", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(LoginActivity.this, "Login Failed", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);
            }
        });

    }

    private User checkUser(List<User> users) {
        for (int i = 0; i < users.size(); i++) {
            User mUser = users.get(i);
            if (mUser.getUserName().equals(etUsername.getText().toString()) && mUser.getUserPassword().equals(etPassword.getText().toString())) {
                return mUser;
            }
        }

        return null;
    }

    private boolean checkValidation() {
        if (ValidationUtils.testEmpty(etUsername.getText().toString())) {
            etUsername.setError("Please enter value");
            etUsername.requestFocus();
            return false;
        }

        if (ValidationUtils.testEmpty(etPassword.getText().toString())) {
            etPassword.setError("Please enter value");
            etPassword.requestFocus();
            return false;
        }

        return true;
    }
}
