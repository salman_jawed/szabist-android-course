package szabist.com.persistence.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import szabist.com.persistence.R;
import szabist.com.persistence.usermanager.User;
import szabist.com.persistence.usermanager.UserManager;
import szabist.com.persistence.utils.ValidationUtils;

/**
 * Created by salmanjawed on 05/11/2017.
 */

public class RegisterActivity extends AppCompatActivity {

    EditText etUserName, etPassword, etEmail;
    Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        etUserName = findViewById(R.id.et_username);
        etEmail = findViewById(R.id.et_email);
        etPassword = findViewById(R.id.et_password);

        btnRegister=findViewById(R.id.btn_register);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkValidation()) {
                    User user = new User();
                    user.setUserEmail(etEmail.getText().toString());
                    user.setUserName(etUserName.getText().toString());
                    user.setUserPassword(etPassword.getText().toString());
                    user.save();

                    UserManager.getInstance(RegisterActivity.this).cacheUserProfileJSON(user);


                    Intent i = new Intent(RegisterActivity.this, HomeActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);

                    finish();
                }
            }
        });
    }

    private boolean checkValidation() {
        if (ValidationUtils.testEmpty(etUserName.getText().toString())) {
            etUserName.setError("Please enter value");
            etUserName.requestFocus();
            return false;
        }

        if (ValidationUtils.testEmpty(etPassword.getText().toString())) {
            etPassword.setError("Please enter value");
            etPassword.requestFocus();
            return false;
        }


        if (ValidationUtils.testEmpty(etEmail.getText().toString())) {
            etEmail.setError("Please enter value");
            etEmail.requestFocus();
            return false;
        }


        if (!ValidationUtils.isEmailValid(etEmail.getText().toString())) {
            etEmail.setError("Please enter valid email");
            etEmail.requestFocus();
            return false;
        }


        return true;
    }
}