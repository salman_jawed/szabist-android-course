package szabist.com.persistence.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import szabist.com.persistence.R;
import szabist.com.persistence.usermanager.UserManager;

public class SplashActivity extends AppCompatActivity {

    String[] permissions = {

            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP){
            requestPermissions();
        } else{
            postPermissionsWork();
        }



    }

    protected void requestPermissions() {
        ActivityCompat.requestPermissions(this, permissions, 1);
    }

    private void postPermissionsWork() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(UserManager.getInstance(SplashActivity.this).getUserProfile()!=null){
                    Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(i);
                    finish();
                }else{
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }


            }
        }, 2000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean granted = false;
        for (int i = 0; i < permissions.length; i++) {
            if (grantResults[i] == PackageManager.PERMISSION_GRANTED) granted = true;
            else {
                granted = false;
                break;
            }
        }

        if (granted) {
            postPermissionsWork();
        } else {
            showPermissionDeniedDialog("These Permissions are needed\nPlease Allow all permissions.");
        }
    }

    private void showPermissionDeniedDialog(String msg) {
        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        requestPermissions();
                        break;
                }
            }
        };
        new AlertDialog.Builder(this)
                .setMessage(msg)
                .setPositiveButton("Ok", listener)
                .create()
                .show();
    }
}
