package szabist.com.persistence.usermanager;

import android.content.Context;

import szabist.com.persistence.AppConstants;
import szabist.com.persistence.utils.GsonUtil;
import szabist.com.persistence.utils.PreferenceHelper;

public class UserManager {

    private static UserManager instance;
    private User user;
    private Context mContext;

    public static UserManager getInstance(Context mContext) {

        if (instance == null) {
            instance = new UserManager(mContext);
        }

//        if (mContext == null) {
//            instance.mContext = mContext;
//        }

        return instance;
    }

    private UserManager(Context context){
        this.mContext=context;
    }

    public void cacheUserProfileJSON(final User user) {

        PreferenceHelper.setPreferenceKey(instance.mContext,
                AppConstants.MY_APP_PREF, AppConstants.USER_MODEL_CACHE_KEY, GsonUtil.getStringFromObject(user));
    }


    public void removeUserProfileJSON() {
        PreferenceHelper.setPreferenceKey(instance.mContext,
                AppConstants.MY_APP_PREF, AppConstants.USER_MODEL_CACHE_KEY, "");
    }


    public User getUserProfile() {
        String userString = PreferenceHelper.getPrefenceKey(instance.mContext,
                AppConstants.MY_APP_PREF, AppConstants.USER_MODEL_CACHE_KEY);

        if (userString != null && userString.length() > 0) {
            return (User) GsonUtil.getObjectFromString(userString, User.class);
        }
        return null;
    }


}
