package szabist.com.persistence.utils;

import android.util.Patterns;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by salman.jawed on 9/1/2015.
 */
public class ValidationUtils {

    public static boolean testEmpty(String str) {
        if ((str == null) || str.matches("^\\s*$")) {
            return true;
        } else {
            if (str.equalsIgnoreCase("null")) {
                return true;
            } else if (str.contains("null")) {
                return true;
            } else {
                return false;
            }
        }
    }


    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches()) {
            isValid = true;
        }

        return isValid;
    }

    public static boolean isValidUrl(String url) {
        return Patterns.WEB_URL.matcher(url).matches();
    }


    public static boolean isNumeric(String str) {
        try {
            @SuppressWarnings("unused")
            int d = Integer.parseInt(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static boolean isNumericFloat(String str) {
        try {
            @SuppressWarnings("unused")
            float f = Float.parseFloat(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static Date getEarlierDate(Date d1,Date d2){
        int compare=d1.compareTo(d2);
        Date smallerDate;
        if(compare==0){
            smallerDate= d1;
        }else if(compare>0){
            smallerDate= d2;
        }else{
            smallerDate= d1;
        }

        return smallerDate;
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//        String smallerDateString = formatter.format(smallerDate);
//        return smallerDateString;
    }
}
