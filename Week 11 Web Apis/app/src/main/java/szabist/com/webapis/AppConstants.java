package szabist.com.webapis;

/**
 * Created by salmanjawed on 12/11/2017.
 */

public class AppConstants {

    public static final String SAMPLE_JSON_ARRAY_GET="https://jsonplaceholder.typicode.com/albums";
    public static final int SAMPLE_JSON_ARRAY_GET_ID=1;

    public static final String SAMPLE_JSON_OBJECT_GET="https://jsonplaceholder.typicode.com/posts/1";
    public static final int SAMPLE_JSON_OBJECT_GET_ID=2;

    public static final String SAMPLE_POST="https://jsonplaceholder.typicode.com/posts";
    public static final int SAMPLE_POST_ID=3;

}
