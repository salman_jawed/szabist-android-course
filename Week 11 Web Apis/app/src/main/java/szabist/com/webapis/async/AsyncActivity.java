package szabist.com.webapis.async;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import szabist.com.webapis.AppConstants;
import szabist.com.webapis.R;
import szabist.com.webapis.responsemodelsasync.Album;
import szabist.com.webapis.responsemodelsasync.Post;

public class AsyncActivity extends AppCompatActivity implements AsyncTaskResponseListener {

    TextView tvResponse, tv_response_object;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async);
        tvResponse = findViewById(R.id.tv_response);
        tv_response_object = findViewById(R.id.tv_response_object);

        /*
        //Get Request Work
        //Json Object
        RetrieveInfoTask mTask = new RetrieveInfoTask(this, this, AppConstants.SAMPLE_JSON_OBJECT_GET_ID, AppConstants.SAMPLE_JSON_OBJECT_GET);

        //Json Array
//        RetrieveInfoTask mTask = new RetrieveInfoTask(this, this, AppConstants.SAMPLE_JSON_ARRAY_GET_ID, AppConstants.SAMPLE_JSON_ARRAY_GET);
        mTask.execute();
        */

        //Post Request Work
        Post mPost = new Post();
        mPost.setId(5);
        mPost.setUserId(5);
        mPost.setTitle("Test Title");
        mPost.setBody("Test Body");
        String requestJson = GsonUtil.getStringFromObject(mPost);

        PostTask task = new PostTask(this, this, AppConstants.SAMPLE_POST_ID, AppConstants.SAMPLE_POST, requestJson);
        task.execute();
    }

    @Override
    public void onResponse(String response, int requestType) {
        tvResponse.setText(response);
        switch (requestType) {
            case AppConstants.SAMPLE_JSON_OBJECT_GET_ID: {
                Post mPost = new Post();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    mPost.setUserId(jsonObject.getInt("userId"));
                    mPost.setId(jsonObject.getInt("id"));
                    mPost.setTitle(jsonObject.getString("title"));
                    mPost.setBody(jsonObject.getString("body"));


                    tv_response_object.setText(mPost.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }

            case AppConstants.SAMPLE_JSON_ARRAY_GET_ID: {

                try {
                    JSONArray jArray = new JSONArray(response);
                    for (int i = 0; i < jArray.length(); i++) {
                        Album mAlbum = new Album();
                        JSONObject jsonObject = jArray.getJSONObject(i);
                        mAlbum.setUserId(jsonObject.getInt("userId"));
                        mAlbum.setId(jsonObject.getInt("id"));
                        mAlbum.setTitle(jsonObject.getString("title"));
                        tv_response_object.setText(tv_response_object.getText().toString() + "\n" + mAlbum.toString());
                    }

//                    tv_response_object.setText(mPost.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
        }

    }

    @Override
    public void onException(int requestType) {
        tvResponse.setText("Error occured");
    }
}
