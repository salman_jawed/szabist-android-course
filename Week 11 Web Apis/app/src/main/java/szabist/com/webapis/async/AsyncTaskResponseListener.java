package szabist.com.webapis.async;

/**
 * Created by salmanjawed on 12/11/2017.
 */

public interface AsyncTaskResponseListener {

    void onResponse(String response, int requestType);

    void onException(int requestType);
}
