package szabist.com.webapis.async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import szabist.com.webapis.utils.UIUtils;

/**
 * Created by salmanjawed on 12/11/2017.
 */

public class PostTask extends AsyncTask<Void, Void, String> {

    private Context mContext;

    private AsyncTaskResponseListener mListener;
    private int mServiceType = -1;
    private String mUrl = null;
    private String requestString;

    public PostTask(Context mContext, AsyncTaskResponseListener mListener, int serviceType, String url, String requestString) {
        this.mContext = mContext;
        this.mListener = mListener;
        this.mServiceType = serviceType;
        this.mUrl = url;
        this.requestString = requestString;
    }

    protected void onPreExecute() {
        //can be done in activity also
        UIUtils.showProgressLoader(mContext);
    }

    protected String doInBackground(Void... urls) {
//        String email = emailText.getText().toString();
        // Do some validation here
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(mUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(15000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);

            DataOutputStream os = new DataOutputStream(urlConnection.getOutputStream());
            os.write(requestString.getBytes());
            os.flush();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line).append("\n");
            }
            bufferedReader.close();

            Log.e("Response to string", stringBuilder.toString());
            return stringBuilder.toString();
        } catch (Exception e) {
            Log.e("ERROR", e.getMessage(), e);
            return null;
        } finally {
            urlConnection.disconnect();
        }

    }

    protected void onPostExecute(String response) {
        UIUtils.hideProgressLoader();
        if (response != null) {
            mListener.onResponse(response, mServiceType);
        } else {
            mListener.onException(mServiceType);
        }

    }
}
