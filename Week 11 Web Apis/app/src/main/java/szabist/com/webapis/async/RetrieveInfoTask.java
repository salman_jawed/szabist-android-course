package szabist.com.webapis.async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import szabist.com.webapis.utils.UIUtils;

/**
 * Created by salmanjawed on 12/11/2017.
 */

public class RetrieveInfoTask extends AsyncTask<Void, Void, String> {

    private Context mContext;

    private AsyncTaskResponseListener mListener;
    private int mServiceType = -1;
    private String mUrl = null;

    public RetrieveInfoTask(Context mContext, AsyncTaskResponseListener mListener, int serviceType, String url) {
        this.mContext = mContext;
        this.mListener = mListener;
        this.mServiceType = serviceType;
        this.mUrl = url;
    }

    protected void onPreExecute() {
        //can be done in activity also
        UIUtils.showProgressLoader(mContext);
    }

    protected String doInBackground(Void... urls) {
//        String email = emailText.getText().toString();
        // Do some validation here

        try {
            URL url = new URL(mUrl);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line).append("\n");
                }
                bufferedReader.close();
                return stringBuilder.toString();
            } finally {
                urlConnection.disconnect();
            }
        } catch (Exception e) {
            Log.e("ERROR", e.getMessage(), e);
            return null;
        }
    }

    protected void onPostExecute(String response) {
        UIUtils.hideProgressLoader();
        if (response != null) {
            mListener.onResponse(response, mServiceType);
        } else {
            mListener.onException(mServiceType);
        }

    }
}
