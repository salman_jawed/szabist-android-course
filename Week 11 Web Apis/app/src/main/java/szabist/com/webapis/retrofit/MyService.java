package szabist.com.webapis.retrofit;

import com.google.gson.JsonElement;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import szabist.com.webapis.responsemodelsasync.Post;

/**
 * Created by salmanjawed on 12/11/2017.
 */

public interface MyService {

    @GET(RetroFitConstants.SAMPLE_JSON_OBJECT_GET+"{postId}")
    Call<JsonElement> getPost(@Path("postId") String postId);

    @GET(RetroFitConstants.SAMPLE_JSON_ARRAY_GET)
    Call<List<JsonElement>> getAlbums();

    @POST(RetroFitConstants.SAMPLE_POST)
    Call<JsonElement> createPost(@Body Post post);
}
