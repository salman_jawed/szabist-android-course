package szabist.com.webapis.retrofit;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import szabist.com.webapis.R;
import szabist.com.webapis.responsemodelsasync.Album;
import szabist.com.webapis.responsemodelsasync.Post;
import szabist.com.webapis.utils.UIUtils;

/**
 * Created by salmanjawed on 12/11/2017.
 */

public class RetroFitActivity extends AppCompatActivity implements RetroFitListener {

    TextView tv_response_object;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retrofit);
        tv_response_object = findViewById(R.id.tv_response_object);
        UIUtils.showProgressLoader(this);

        //simple get
//        RetroFitRequests.getPosts("1", RetroFitConstants.SAMPLE_JSON_OBJECT_GET_ID,Post.class, this);
        //array get
//        RetroFitRequests.getAlbums(RetroFitConstants.SAMPLE_JSON_ARRAY_GET_ID, Album[].class, this);

        //post retrofit
        Post mPost = new Post();
        mPost.setId(173);
        mPost.setUserId(10);
        mPost.setTitle("Test Title Nowww");
        mPost.setBody("Test Body Nowww");
        RetroFitRequests.createPost(mPost, RetroFitConstants.SAMPLE_POST_ID, Post.class, this);
    }

    @Override
    public void onResponse(Object responseObject, int requestType) {
        UIUtils.hideProgressLoader();
        switch (requestType) {
            case RetroFitConstants.SAMPLE_JSON_OBJECT_GET_ID: {
                Post mPost = (Post) responseObject;
                tv_response_object.setText(mPost.toString());
                break;
            }
            case RetroFitConstants.SAMPLE_JSON_ARRAY_GET_ID: {
                Album[] mList = (Album[]) responseObject;
                for (int i = 0; i < mList.length; i++) {
                    tv_response_object.setText(tv_response_object.getText().toString() + "\n" + mList[i].toString());
                }
                break;
            }
            case RetroFitConstants.SAMPLE_POST_ID: {
                Post mPost = (Post) responseObject;
                tv_response_object.setText(mPost.toString());
                break;
            }
        }
    }

    @Override
    public void onException(String error, int requestType) {
        UIUtils.hideProgressLoader();
        tv_response_object.setText(error);
    }


}
