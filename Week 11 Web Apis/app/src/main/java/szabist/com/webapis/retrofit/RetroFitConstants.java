package szabist.com.webapis.retrofit;

/**
 * Created by salmanjawed on 12/11/2017.
 */

public class RetroFitConstants {

    public static final String BASEURL="https://jsonplaceholder.typicode.com";


    public static final String SAMPLE_JSON_ARRAY_GET="albums";
    public static final int SAMPLE_JSON_ARRAY_GET_ID=1;

    public static final String SAMPLE_JSON_OBJECT_GET="posts/";
    public static final int SAMPLE_JSON_OBJECT_GET_ID=2;

    public static final String SAMPLE_POST="posts";
    public static final int SAMPLE_POST_ID=3;

}
