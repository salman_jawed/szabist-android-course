package szabist.com.webapis.retrofit;

/**
 * Created by salmanjawed on 12/11/2017.
 */

public interface RetroFitListener {

    void onResponse(Object responseObject, int requestType);

    void onException(String error,int requestType);
}
