package szabist.com.webapis.retrofit;

import com.google.gson.JsonElement;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import szabist.com.webapis.async.GsonUtil;
import szabist.com.webapis.responsemodelsasync.Post;

/**
 * Created by salmanjawed on 12/11/2017.
 */

public class RetroFitRequests {

    private static Retrofit mRetroFit = null;
    private static MyService mService = null;

    private static final Retrofit getRetroFit() {
        if (mRetroFit == null) {
            mRetroFit = new Retrofit.Builder()
                    .baseUrl(RetroFitConstants.BASEURL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return mRetroFit;
    }

    private static final MyService getService() {
        if (mService == null) {
            mService = getRetroFit().create(MyService.class);
        }
        return mService;
    }

    public static final void getPosts(String postId, final int serviceType, final Class mClass, final RetroFitListener listener) {
        Call<JsonElement> call = getService().getPost(postId);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if (response.isSuccessful()) {
                    listener.onResponse(GsonUtil.getObjectFromString(response.body().toString(), mClass), serviceType);
                } else {
                    listener.onException(response.errorBody().toString(), serviceType);
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                listener.onException(t.getLocalizedMessage(), serviceType);
            }
        });

    }

    public static final void getAlbums(final int serviceType, final Class mClass, final RetroFitListener listener) {
        Call<List<JsonElement>> call = getService().getAlbums();
        call.enqueue(new Callback<List<JsonElement>>() {
            @Override
            public void onResponse(Call<List<JsonElement>> call, Response<List<JsonElement>> response) {
                if (response.isSuccessful()) {
                    listener.onResponse(GsonUtil.getObjectFromString(response.body().toString(), mClass), serviceType);
                } else {
                    listener.onException(response.errorBody().toString(), serviceType);
                }
            }

            @Override
            public void onFailure(Call<List<JsonElement>> call, Throwable t) {
                listener.onException(t.getLocalizedMessage(), serviceType);
            }
        });

    }

    public static final void createPost(Post mPost, final int serviceType, final Class mClass, final RetroFitListener listener) {
        Call<JsonElement> call = getService().createPost(mPost);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if (response.isSuccessful()) {
                    listener.onResponse(GsonUtil.getObjectFromString(response.body().toString(), mClass), serviceType);
                } else {
                    listener.onException("failed", serviceType);
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                listener.onException(t.getLocalizedMessage(), serviceType);
            }
        });

    }

}
