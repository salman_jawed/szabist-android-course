package szabist.com.webapis.utils;

import android.app.Dialog;
import android.content.Context;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import java.util.concurrent.atomic.AtomicInteger;

import szabist.com.webapis.R;

/**
 * Created by salman.jawed on 9/22/2015.
 */
public class MyProgressDialog extends Dialog {

    public static AtomicInteger progressDialogCounter = new AtomicInteger(0);

    public MyProgressDialog show(Context context, CharSequence title,
                                 CharSequence message) {
        return show(context, title, message, false);
    }

    public MyProgressDialog show(Context context, CharSequence title,
                                 CharSequence message, boolean indeterminate) {
        return show(context, title, message, indeterminate, false, null);
    }

    public MyProgressDialog show(Context context, CharSequence title,
                                 CharSequence message, boolean indeterminate, boolean cancelable) {
        return show(context, title, message, indeterminate, cancelable, null);
    }

    public MyProgressDialog show(Context context, CharSequence title,
                                 CharSequence message, boolean indeterminate, boolean cancelable,
                                 OnCancelListener cancelListener) {
        MyProgressDialog dialog = new MyProgressDialog(context);
        dialog.setTitle(title);
        dialog.setCancelable(cancelable);
        dialog.setOnCancelListener(cancelListener);
        //The next line will add the ProgressBar to the dialog.
        dialog.addContentView(new ProgressBar(context), new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        dialog.show();

        return dialog;
    }

    public MyProgressDialog(Context context) {
        super(context, R.style.NewDialog);
    }

}
