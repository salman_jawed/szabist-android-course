package szabist.com.webapis.utils;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import szabist.com.webapis.R;


/**
 * Created by salman.jawed on 9/1/2015.
 */
public class NetworkUtil {

    public static boolean isConnected(Object context, boolean showDialogBox) {
        boolean HaveConnectedWifi = false;
        boolean HaveConnectedMobile = false;
        Context localContextObject = null;
        Fragment localFragment;
        if (context instanceof Fragment) {
            localFragment = (Fragment) context;
            localContextObject = localFragment.getActivity();
        } else if (context instanceof Activity) {
            localContextObject = (Context) context;
        } else if (context instanceof Context) {
            localContextObject = (Context) context;
        }

        ConnectivityManager cm = (ConnectivityManager) localContextObject.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    HaveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    HaveConnectedMobile = true;
        }
        if (!HaveConnectedMobile && !HaveConnectedWifi && showDialogBox) {
            UIUtils.showErrorDialog(localContextObject,
                    localContextObject.getString(R.string.please_check_your_internet_connection),
                    localContextObject.getString(R.string.no_network_access));
        }

        //showToast(context, context.getString(R.string.no_network_access));
        //showErrorDialog(context,context.getString(R.string.no_network_access),

	/*	else if(!HaveConnectedMobile && !HaveConnectedWifi && displayCacheMsg)
            showToast(context, context.getString(R.string.display_date_from_cache));*/
        return HaveConnectedWifi || HaveConnectedMobile;
    }


}
