package szabist.com.webapis.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import szabist.com.webapis.R;


/**
 * Created by salman.jawed on 9/1/2015.
 */
public class UIUtils {


    static MyProgressDialog globalProgressDialog;


    public static void showErrorDialog(Context context, String message, String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton(
                        context.getResources().getString(R.string.lbl_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        builder.setView(null);

        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void showDialog(Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton(
                        context.getResources().getString(R.string.lbl_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        builder.setView(null);

        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * Hide soft keyboard
     */
    public static void hideSoftKeyboard(Activity activity, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public static void showToastLong(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static void showToastShort(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static String generateMaskedString(String str) {
        StringBuilder maskedStringBuffer = new StringBuilder();
        if (str != null && str.length() > 0) {
            maskedStringBuffer.append(str.charAt(0));
            for (int i = 1; i < str.length(); i++) {
                maskedStringBuffer.append("x");
            }
        }
        return maskedStringBuffer.toString();
    }

    public static String generateFullMaskedString(String str) {
        StringBuilder maskedStringBuffer = new StringBuilder();
        if (str != null && str.length() > 0) {
            for (int i = 0; i < str.length(); i++) {
                maskedStringBuffer.append("x");
            }
        }
        return maskedStringBuffer.toString();
    }


    public static void showProgressLoader(Context context) {
        try {
            globalProgressDialog = new MyProgressDialog(context);
            if (globalProgressDialog.progressDialogCounter.get() < 1) {
                globalProgressDialog = globalProgressDialog.show(context,
                        null, "Please wait...", true);
            }

            //increment counter
            globalProgressDialog.progressDialogCounter.getAndIncrement();
        } catch (WindowManager.BadTokenException e) {

        } catch (Exception e) {

        }
    }

    public static void hideProgressLoader() {
        if (globalProgressDialog.progressDialogCounter.get() > 1) {
            globalProgressDialog.progressDialogCounter.decrementAndGet();
        } else {
            if (globalProgressDialog != null) {
                globalProgressDialog.cancel();
                globalProgressDialog.dismiss();
            }
            globalProgressDialog.progressDialogCounter.set(0);
        }
    }

    public static void restartActivity(Activity myActivity) {
        Intent intent = myActivity.getIntent();
//        intent.putExtra("carousel_position", carousel_position);
        myActivity.finish();
        myActivity.startActivity(intent);
//        myActivity.overridePendingTransition(animIn, animOut);
    }
}
