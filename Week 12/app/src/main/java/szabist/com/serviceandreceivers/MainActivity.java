package szabist.com.serviceandreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    TextView tvCount;
    Button btnBroadcast;
    Button btnService;

    int i = 0;

    MyReceiver mReceiver = null;
    boolean mIsReceiverRegistered = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        tvCount = findViewById(R.id.tv_count);
        btnBroadcast = findViewById(R.id.btn_broadcast);
        btnService = findViewById(R.id.btn_service);

        tvCount.setText(i + "");

        btnBroadcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                broadcastIntent();
            }
        });

        btnService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                serviceWork();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mIsReceiverRegistered) {
            unregisterReceiver(mReceiver);
            mReceiver = null;
            mIsReceiverRegistered = false;
        }

        // Other onPause() code here


    }

    public void serviceWork() {
        Intent i = new Intent(this, MyService.class);
        // potentially add data to the intent

        startService(i);
    }

    public void broadcastIntent() {
        Intent intent = new Intent();
        intent.setAction("szabist.com.serviceandreceivers");
        sendBroadcast(intent);
    }

    private void updateUi() {
        i++;
        tvCount.setText(i + "");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mIsReceiverRegistered) {
            if (mReceiver == null)
                mReceiver = new MyReceiver();
            registerReceiver(mReceiver, new IntentFilter("szabist.com.serviceandreceivers"));
            mIsReceiverRegistered = true;
        }
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(context, "Broadcasted", Toast.LENGTH_SHORT).show();
            updateUi();
        }
    }

}

