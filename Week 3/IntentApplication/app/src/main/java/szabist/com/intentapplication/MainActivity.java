package szabist.com.intentapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText etName;
    Button btnSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etName = (EditText) findViewById(R.id.et_name);
        btnSend = (Button) findViewById(R.id.btn_send);


        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etName.getText().toString().length() > 0) {
                    Bundle b = new Bundle();
                    b.putString("name", etName.getText().toString() + "");

                    Intent i=new Intent(MainActivity.this,SecondActivity.class);
                    i.putExtras(b);
                    startActivity(i);
                } else {
                    etName.setError(getString(R.string.enter_name));
                }

            }
        });

    }
}
