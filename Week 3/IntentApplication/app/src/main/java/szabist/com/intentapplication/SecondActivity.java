package szabist.com.intentapplication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by salmanjawed on 17/09/2017.
 */

public class SecondActivity extends AppCompatActivity {

    TextView tvName;
    String name;
    Bundle b;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_second);

        tvName= (TextView) findViewById(R.id.tv_name);

        b=getIntent().getExtras();

        name=b.getString("name");

        tvName.setText(name);

    }

    }
