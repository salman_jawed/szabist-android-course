package szabist.com.week3application;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by Test on 9/23/2017.
 */

public class SecondActivity extends AppCompatActivity {

    TextView tvValue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two);

        tvValue= (TextView) findViewById(R.id.tv_value);

        Bundle b=getIntent().getExtras();

        String name=b.getString("name");

        tvValue.setText(name+"");

    }
}
