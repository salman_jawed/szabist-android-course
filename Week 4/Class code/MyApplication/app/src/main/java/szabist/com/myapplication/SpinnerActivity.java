package szabist.com.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SpinnerActivity extends AppCompatActivity {

    Spinner spCountry;
    ArrayList<ModelCountry> CountryList;
    SpinnerAdapter spinnerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_spinner);

        spCountry = (Spinner) findViewById(R.id.sp_country);
        CountryList = new ArrayList<>();

        CountryList = getCountries();

        spinnerAdapter = new SpinnerAdapter(this,CountryList);
        spCountry.setAdapter(spinnerAdapter);

        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                Toast.makeText(SpinnerActivity.this, CountryList.get(position).getCountryName(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private ArrayList<ModelCountry> getCountries(){
        ArrayList<ModelCountry> CountryList=new ArrayList<>();

        ModelCountry modelCountry = new ModelCountry();;
        modelCountry.setCountryId("-1");
        modelCountry.setCountryName("Select Country");
        CountryList.add(modelCountry);

                modelCountry = new ModelCountry();;
        modelCountry.setCountryId("1");
        modelCountry.setCountryName("Pakistan");
        CountryList.add(modelCountry);

        modelCountry = new ModelCountry();;
        modelCountry.setCountryId("2");
        modelCountry.setCountryName("Australia");
        CountryList.add(modelCountry);

        modelCountry = new ModelCountry();;
        modelCountry.setCountryId("3");
        modelCountry.setCountryName("USA");
        CountryList.add(modelCountry);

        modelCountry = new ModelCountry();;
        modelCountry.setCountryId("4");
        modelCountry.setCountryName("Turkey");
        CountryList.add(modelCountry);

        return CountryList;
    }
}
