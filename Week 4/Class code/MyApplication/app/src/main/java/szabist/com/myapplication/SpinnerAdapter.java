package szabist.com.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Test on 10/7/2017.
 */

public class SpinnerAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<ModelCountry> countryList;
    LayoutInflater mInflater;

    public SpinnerAdapter(Context context, ArrayList<ModelCountry> countries) {
        mContext = context;
        countryList = countries;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return countryList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View mView = mInflater.inflate(R.layout.row_spinner,null);

        TextView tv = (TextView) mView.findViewById(R.id.tv_row_spinner);

        tv.setText(countryList.get(position).getCountryName());
        return mView;
    }
}
