package szabist.com.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class SpinnerActivity extends AppCompatActivity {

    Spinner spGender;
    //Data Array for spinner
    ArrayList<ModelSpinner> genderList = new ArrayList<>();

    //Adapter for spinner
    SpinnerAdapter spinnerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);
        //binding spinner from ui to java
        spGender = (Spinner) findViewById(R.id.sp_gender);

        //initialize the gender list
        genderList = initGenderList();

        //binding adapter to spinner
        spinnerAdapter = new SpinnerAdapter(this, genderList);
        spGender.setAdapter(spinnerAdapter);

        //item selected listener for spinner
        spGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(SpinnerActivity.this, genderList.get(i).getGenderText() + " is selected", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spGender.setSelection(1);

    }

    private ArrayList<ModelSpinner> initGenderList() {

        ArrayList<ModelSpinner> tempList = new ArrayList<>();

        ModelSpinner mModel = new ModelSpinner();
        mModel.setGenderId("-1");
        mModel.setGenderText("Select Gender");
        tempList.add(mModel);

        mModel = new ModelSpinner();
        mModel.setGenderId("1");
        mModel.setGenderText("Male");
        tempList.add(mModel);

        mModel = new ModelSpinner();
        mModel.setGenderId("2");
        mModel.setGenderText("Female");
        tempList.add(mModel);

        return tempList;
    }
}
