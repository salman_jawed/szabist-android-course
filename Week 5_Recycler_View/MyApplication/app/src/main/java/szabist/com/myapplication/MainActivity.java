package szabist.com.myapplication;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<ModelViewPager> pagerList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);


        pagerList = new ArrayList<>();
        pagerList = getPagerItems();

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new CustomPagerAdapter(this, pagerList));
    }

    private ArrayList<ModelViewPager> getPagerItems() {
        ArrayList<ModelViewPager> mList = new ArrayList<>();

        ModelViewPager mModel = new ModelViewPager();
        mModel.setTitle("Picture 1");
        mModel.setImageUrl("https://i.ytimg.com/vi/3v5Q_dPQUYo/maxresdefault.jpg");
        mList.add(mModel);

        mModel = new ModelViewPager();
        mModel.setTitle("Picture 2");
        mModel.setImageUrl("http://www.sourcecertain.com/img/Example.png");
        mList.add(mModel);

        mModel = new ModelViewPager();
        mModel.setTitle("Picture 3");
        mModel.setImageUrl("http://familyeyecaretimmins.com/wp-content/uploads/2016/04/depositphotos_8938809-Example-rubber-stamp.jpg");
        mList.add(mModel);

        mModel = new ModelViewPager();
        mModel.setTitle("Picture 4");
        mModel.setImageUrl("https://i.pinimg.com/736x/55/f7/1f/55f71f74a0cd21a81694d172072d6538--lead-by-example-classroom-quotes.jpg");
        mList.add(mModel);

        return mList;
    }
}
