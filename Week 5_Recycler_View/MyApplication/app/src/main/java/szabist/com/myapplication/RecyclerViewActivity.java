package szabist.com.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class RecyclerViewActivity extends AppCompatActivity {

    ArrayList<ModelRecyclerView> recyclerViewArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);


        recyclerViewArrayList = new ArrayList<>();
        recyclerViewArrayList = getRecyclerViewItems();

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager mManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mManager);


        RecyclerViewAdapter mAdap = new RecyclerViewAdapter(recyclerViewArrayList, getApplicationContext());
        recyclerView.setAdapter(mAdap);
    }

    private ArrayList<ModelRecyclerView> getRecyclerViewItems() {
        ArrayList<ModelRecyclerView> mList = new ArrayList<>();

        ModelRecyclerView mModel = new ModelRecyclerView();
        mModel.setMovieId("1");
        mModel.setMovieTitle("Mad Max: Fury Road");
        mModel.setMovieImage("http://movie.info/mh/mad-max-fury-road.jpg");
        mModel.setMovieGenre("Action");
        mModel.setMovieDescription("Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum");
        mList.add(mModel);

        mModel = new ModelRecyclerView();
        mModel.setMovieId("2");
        mModel.setMovieTitle("Furious 7");
        mModel.setMovieImage("http://movie.info/mh/fast-and-furious-7_.jpg");
        mModel.setMovieGenre("Action");
        mModel.setMovieDescription("Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum");
        mList.add(mModel);

        mModel = new ModelRecyclerView();
        mModel.setMovieId("3");
        mModel.setMovieTitle("Star Wars: Episode VII - The Force Awakens");
        mModel.setMovieImage("http://movie.info/mh/star-wars-the-force-awakens.jpg");
        mModel.setMovieGenre("Action");
        mModel.setMovieDescription("Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum");
        mList.add(mModel);

        mModel = new ModelRecyclerView();
        mModel.setMovieId("4");
        mModel.setMovieTitle("Mad Max: Fury Road");
        mModel.setMovieImage("http://movie.info/mh/mad-max-fury-road.jpg");
        mModel.setMovieGenre("Action");
        mModel.setMovieDescription("Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum");
        mList.add(mModel);

        mModel = new ModelRecyclerView();
        mModel.setMovieId("5");
        mModel.setMovieTitle("Furious 7");
        mModel.setMovieImage("http://movie.info/mh/fast-and-furious-7_.jpg");
        mModel.setMovieGenre("Action");
        mModel.setMovieDescription("Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum");
        mList.add(mModel);

        mModel = new ModelRecyclerView();
        mModel.setMovieId("6");
        mModel.setMovieTitle("Star Wars: Episode VII - The Force Awakens");
        mModel.setMovieImage("http://movie.info/mh/star-wars-the-force-awakens.jpg");
        mModel.setMovieGenre("Action");
        mModel.setMovieDescription("Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum");
        mList.add(mModel);


        return mList;
    }
}
