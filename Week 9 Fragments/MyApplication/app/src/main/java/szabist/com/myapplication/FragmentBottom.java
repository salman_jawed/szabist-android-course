package szabist.com.myapplication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by salmanjawed on 29/10/2017.
 */

public class FragmentBottom extends Fragment {

    View mView;
    EditText etMain;
    Button btnBottom;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_bottom, null);
        etMain = (EditText) mView.findViewById(R.id.et_main);
        btnBottom = (Button) mView.findViewById(R.id.btn_main_bottom);

        btnBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Click Bottom => " + etMain.getText().toString(), Toast.LENGTH_SHORT).show();
                FragmentBottomSecond secondFrag = new FragmentBottomSecond();
                Bundle b = new Bundle();
                b.putString("value", etMain.getText().toString());
                secondFrag.setArguments(b);
                ((MainActivity) getActivity()).replaceFragmentBottom(secondFrag);
            }
        });
        return mView;
    }
}
