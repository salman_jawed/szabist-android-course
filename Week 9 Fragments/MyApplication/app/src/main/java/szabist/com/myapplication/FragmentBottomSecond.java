package szabist.com.myapplication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by salmanjawed on 29/10/2017.
 */

public class FragmentBottomSecond extends Fragment {

    View mView;
    TextView tvValue;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_bottom_second, null);

        tvValue = (TextView) mView.findViewById(R.id.tv_value);

        Bundle b = getArguments();
        tvValue.setText("Value =>" + b.getString("value"));


        return mView;
    }
}
