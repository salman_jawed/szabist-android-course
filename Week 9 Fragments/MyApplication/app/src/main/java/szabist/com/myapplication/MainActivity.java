package szabist.com.myapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity {

    FrameLayout frameTop;
    FrameLayout frameBottom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        frameTop = (FrameLayout) findViewById(R.id.frame_top);
        frameBottom = (FrameLayout) findViewById(R.id.frame_bottom);


        FragmentTop topFragment = new FragmentTop();
        FragmentBottom bottomFragment = new FragmentBottom();

        getSupportFragmentManager().beginTransaction().add(R.id.frame_top, topFragment).commit();
        getSupportFragmentManager().beginTransaction().add(R.id.frame_bottom, bottomFragment).commit();
    }


    public void addFragmentBottom(Fragment mFrag) {
        getSupportFragmentManager().
                beginTransaction().//begin new fragment transaction
                replace(R.id.frame_bottom, mFrag).//replace the current view
                setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).//animation for chagne
                addToBackStack(null).//adding to stack of bottom frame
                commit();
    }

    public void replaceFragmentBottom(Fragment mFrag) {
        getSupportFragmentManager().
                beginTransaction().//begin new fragment transaction
                replace(R.id.frame_bottom, mFrag).//replace the current view
                setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).//animation for chagne
                commit();
    }
}
